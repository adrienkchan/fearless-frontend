function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card-spacer-y card mb-4 shadow-lg p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</p>
          <p class="card-text">${description}</p>
          <p class="card-footer text-muted">${start} - ${end}</p>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        document.querySelector('.row').innerHTML += `
        <div class="alert alert-danger" role="alert">
            There was a problem gathering information.
        </div>
        `
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toLocaleDateString('en-US');
            const ends = new Date(details.conference.ends).toLocaleDateString('en-US');
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e);
    }

  });
